// Fetches the electron server
const server =
  typeof localStorage !== 'undefined'
    ? localStorage.getItem('api-host') ?? ''
    : null;
type Method = 'GET' | 'POST' | 'PATCH' | 'DELETE' | 'HEAD';
export const fetchSrv = (
  method: Method,
  route: string,
  body?: string,
  headers?: Record<string, string>,
  init?: RequestInit,
) =>
  fetch(
    `${server ? 'http://' + server : '/api'}${
      route.startsWith('/') ? '' : '/'
    }${route}`,
    {
      method,
      body,
      headers: {
        'X-Desired-Cors': `${location.host} ${location.hostname} ${location.origin}`,
        ...(headers ?? {}),
        ...(init?.headers ?? {}),
      },
      ...(init ?? {}),
    },
  );
export default fetchSrv;
