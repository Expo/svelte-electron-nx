import type { IconOrIconConsts } from '@3xpo/svelte-bootstrap-ico';

export const Groups = {
  '/(home)/': ['Home', '/', 'house'],
  '/(home)/tools': ['Tools', '/tools', 'tools'],
  '/(home)/repos': ['Repos', '/repos', 'git'],
} satisfies Record<string, [string, string, IconOrIconConsts]>;

export type GroupPath = keyof typeof Groups;
export type Group = (typeof Groups)[keyof typeof Groups];

export default Groups as Record<GroupPath, Group>;
