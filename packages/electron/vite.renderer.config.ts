import { defineConfig } from 'vite';

// https://vitejs.dev/config
export default defineConfig({
  root: process.cwd() + '/src',
  build: {
    outDir: process.cwd() + '/dist',
    rollupOptions: {
      input: {
        app: './src/index.html',
      },
    },
  },
  server: {
    port: 6173,
    strictPort: true,
  },
});
