import type { ForgeConfig } from '@electron-forge/shared-types';
import { MakerSquirrel } from '@electron-forge/maker-squirrel';
import { MakerZIP } from '@electron-forge/maker-zip';
import { MakerDeb } from '@electron-forge/maker-deb';
import { MakerRpm } from '@electron-forge/maker-rpm';
import { VitePlugin } from '@electron-forge/plugin-vite';
import { cpSync, existsSync, rmSync } from 'fs';

// import { linkSync, rmSync, existsSync, symlinkSync } from 'fs';

// if (existsSync('src/static'))
//   rmSync('src/static', {
//     recursive: true,
//     force: true,
//   });
// symlinkSync('../../svelte/build', process.cwd() + '/src/static');

if (existsSync('node_modules'))
  rmSync(__dirname + '/node_modules', {
    recursive: true,
    force: true,
  });

const config: ForgeConfig = {
  packagerConfig: {},
  rebuildConfig: {},
  makers: [
    new MakerSquirrel({}),
    new MakerZIP({}, ['darwin']),
    // new MakerRpm({}),
    new MakerDeb({}),
  ],
  plugins: [
    new VitePlugin({
      // `build` can specify multiple entry builds, which can be Main process, Preload scripts, Worker process, etc.
      // If you are familiar with Vite configuration, it will look really familiar.
      build: [
        {
          entry: 'src/main.ts',
          config: 'vite.main.config.ts',
        },
        {
          entry: 'src/preload.ts',
          config: 'vite.preload.config.ts',
        },
      ],
      renderer: [
        {
          name: 'main_window',
          config: 'vite.renderer.config.ts',
        },
      ],
    }),
  ],
  hooks: {
    postPackage: async (forgeConfig, options) => {
      console.info('Packages built at:', options.outputPaths);
      options.outputPaths.forEach(v => {
        cpSync(
          __dirname + '/../svelte/build',
          v + '/resources/app/dist/static',
          {
            recursive: true,
          },
        );
        cpSync(
          __dirname + '/../svelte/build',
          v + '/resources/app/src/static',
          {
            recursive: true,
          },
        );
      });
    },
  },
};

export default config;
