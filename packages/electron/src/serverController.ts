// Controls the server, restarting it on change

import path from 'path';
import Koa from 'koa';
import koaSend from 'koa-send';
import koaStatic from 'koa-static';
import type { BrowserWindow } from 'electron';
import type { Server, IncomingMessage, ServerResponse } from 'http';
import configSrv from './server';
import { execSync } from 'child_process';
import { existsSync } from 'fs';

export default async (mainWindow: BrowserWindow) => {
  let server: Server<typeof IncomingMessage, typeof ServerResponse>;
  const listen = async (port = 0) => {
    const app = new Koa();
    const root = path.join(__dirname, 'static');
    const electronVite = MAIN_WINDOW_VITE_DEV_SERVER_URL;
    const svelteVite = electronVite ? 'http://localhost:15999' : null;
    const corsMiddleware = ((ctx, nx) => {
      const ip = ctx.ip ?? '255.255.255.255';
      const orig =
        ctx.req.headers.origin ?? ctx.req.headers.referer ?? 'http://0.0.0.0:'; // default is trusted as browsers cannot spoof & we block all non-local connections anyway

      ctx.res.setHeader(
        'access-control-allow-origin',
        `${ctx.req.headers['X-Desired-Cors'] ?? orig ?? 'none'}`,
      );
      ctx.res.setHeader(
        'access-control-allow-methods',
        `${ctx.method} GET POST`,
      );

      const origIsLocal =
        orig.startsWith('http://127.0.0.1:') ||
        orig.startsWith('http://localhost:') ||
        orig.startsWith('http://0.0.0.0:');

      if (ip !== '127.0.0.1' && ip !== '::') {
        // 400 but with more trollage
        ctx.body = 'ERR_INV' + '\0'.repeat(32768);
        ctx.status = 491;
        ctx.res.setHeader('x-fuck-off', 'fuck off mate');
        return;
      }
      if (ctx.header['sec-fetch-site'] === 'cors' && !origIsLocal) {
        ctx.body = JSON.stringify(
          {
            success: false,
            error: '~ Unauthorized ~',
            origin: orig,
            userIp: ip,
          },
          null,
          2,
        );
        ctx.res.setHeader('content-type', 'application/json');
        ctx.status = 401;

        console.warn('401');

        return;
      }

      ctx.res.setHeader(
        'access-control-allow-methods',
        `${ctx.method}, OPTIONS, GET, POST, HEAD, PATCH, DELETE`,
      );
      ctx.res.setHeader('access-control-allow-credentials', 'true');
      ctx.res.setHeader('access-control-allow-headers', '*');

      if (ctx.method === 'OPTIONS') {
        ctx.body = '';
        return;
      }

      return nx();
    }) satisfies Parameters<typeof app.use>[0];
    app.use(corsMiddleware);
    if (electronVite) {
      mainWindow.loadURL(electronVite);

      const chokidar = await import('chokidar');
      const build = () =>
        execSync(
          'esbuild --bundle --minify --format=cjs src/server/index.ts',
        ).toString('utf-8');
      const watcher = chokidar.watch(process.cwd() + '/src/server', {
        depth: 16,
        followSymlinks: false,
      });
      let restartTimeout: ReturnType<typeof setTimeout> | undefined = undefined;
      let started = false;
      const last = build();
      const listener = () => {
        if (!started) return;
        if (restartTimeout) clearTimeout(restartTimeout);
        if (last === build()) return;
        restartTimeout = setTimeout(() => {
          watcher.off('all', listener);
          started = false;
          const j = server.address();
          listen(port || (typeof j === 'string' ? undefined : j.port)).catch(
            v => {
              console.error(v);
              throw new Error('Failed to restart on change');
            },
          );
        }, 100);
      };
      watcher.on('all', listener);
      setTimeout(() => {
        started = true;
      }, 500);

      const src = `return (async()=>{
  const m = {
    id: '<internal>',
    path: '.',
    exports: {},
    filename: 'index.ts',
    loaded: true,
    children: [],
    paths: [],
  }
  await(async (module)=>{
${last}
  })(m);
  return m.exports;
})();
`;
      try {
        const returnValue = await (new Function(src)() as Promise<
          typeof import('./server')
        >);

        returnValue.default(app);
      } catch (error) {
        console.error('Encountered error while executing');
        console.log(src);
        throw error;
      }

      app.use(async (ctx, nx) => {
        // if (ctx.status === 404) {
        // const rs = await fetch(svelteVite + ctx.path, {
        //   method: ctx.method,
        //   body: ctx.body,
        //   headers: ctx.req.headers as any,
        //   referrer: ctx.req.headers.referer,
        // });

        // if (rs.status !== 404) {
        //   ctx.body = await rs.text();
        //   ctx.status = rs.status;
        //   rs.headers.forEach((v, k) => ctx.res.setHeader(k, v));
        // }
        // }
        if (ctx.path === '/load') {
          const target =
            svelteVite + '/.init/' + encodeURIComponent(ctx.req.headers.host);
          // await mainWindow.loadURL(target);
          return ctx.redirect(target);
        }
        return await nx();
      });
    } else {
      mainWindow.loadFile(path.join(__dirname, 'index.html'));

      app.use(
        koaStatic(root, {
          maxAge: 2048,
          defer: false,
        }),
      );

      app.use(async (ctx, nx) => {
        if (ctx.path.startsWith('/load')) return ctx.redirect('/');
        if (!ctx.path.startsWith('/api/') && ctx.status === 404) {
          return await koaSend(ctx, 'index.html', {
            root,
          });
        }
        ctx.path = ctx.path.replace('/api', '');
        return await nx();
      });

      await configSrv(app);
    }
    if (typeof server !== 'undefined')
      await new Promise((rs, rj) =>
        server.close(e => (e ? rj(e) : rs(void 0))),
      );
    server = app.listen(port, '127.0.0.1');
    let address = server.address();
    if (!address && !port) {
      setTimeout(() => {
        server.close();
        mainWindow.close();
        process.exit(1);
      }, 10);
      throw new Error('Got no address');
    } else if (port) address = `http://127.0.0.1:${port}`;
    const addy =
      typeof address === 'string'
        ? address
        : address.address
          ? `http://${
              address.family === 'ipv6'
                ? `[${address.address}]`
                : `${address.address}`
            }:${address.port}`
          : `http://127.0.0.1:${address.port}`;
    const startWait = performance.now();
    let lastLog = startWait;
    if (svelteVite)
      while (
        await fetch(svelteVite)
          .then(r => !r.ok)
          .catch(() => true)
      ) {
        await new Promise(rs => setTimeout(rs, 10));
        if (performance.now() - lastLog > 1000) {
          console.warn(
            `WARN: Waiting for svelte (${svelteVite}) start for`,
            ((performance.now() - startWait) / 1000).toFixed(0) + ' seconds',
          );
          lastLog = performance.now();
        }
      }
    mainWindow.loadURL(addy + '/load');

    mainWindow.on('close', () => server.close());

    console.log(`Listening on ${addy}`);
  };
  const tryPorts = async (tryPort = 0, depth = 0): Promise<number> => {
    const tempKoa = new Koa();
    const tempServer = tempKoa.listen(tryPort);
    await new Promise(rs => setTimeout(rs, 10));
    const ad = tempServer.address();
    const port = typeof ad === 'string' ? null : 'port' in ad ? ad.port : null;
    tempServer.close();
    if (!port) {
      if (depth > 128) {
        console.warn(
          `Tried 128 ports, failed all 128, attempting to listen on ${tryPort} anyway.`,
        );

        return tryPort;
      }
      if (tryPort === 0) tryPort = 1024 + Math.floor(Math.random() * 4096);
      return await tryPorts(tryPort + 1, depth + 1);
    }
    return port;
  };
  const port = await tryPorts();
  await listen(port ?? 23189);
};
