// This file configures the server
export default async (app: import('koa')) => {
  app.use((ctx, nx) => {
    if (ctx.path.startsWith('/tools')) {
      ctx.body = 'Hi!';
    }
    return nx();
  });
};
