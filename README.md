# template-electron-sk

A template for SvelteKit + Electron + Nx + Koa + TypeScript.

## Stack

This uses the `SkENSTTK` Stack;

- **S**velte
- Svelte**k**it
- **E**lectron
- **N**x
- **S**keleton
- **T**ailwind
- **T**ypescript
- **K**oa

## Warning

Any program on your device, who can access it via localhost, can interact with the Koa instance in the same way you can, meaning they can likely escalate to the user you run as, if you decide to have shitty code.

I do disallow external connections directly to the server, and I disallow any CORS connections that aren't from a site that reports itself as `localhost`, however you shouldn't just like directly expose `execSync`.

## Replace Strings

To setup, clone the repo, and replace the following with your names:

- `template-electron-sk` -> Lowercase name with only a-z0-9 and dashes.
- `SkENSTTK` -> Short Name
- `SvelteKit + Electron + Nx + Skeleton + Tailwind + TypeScript + Koa Template` -> Displayed Name
- `A template for SvelteKit + Electron + Nx + Koa + TypeScript.` -> Description
